package bucket

import (
	"context"

	"github.com/rs/zerolog"
	"gitlab.com/oldmanswar/gotick/pkg/tools"
)

type azureExplorer struct {
	logger *zerolog.Logger
	keeper *Keeper
}

func newAzureExplorer(ctx context.Context, keeper *Keeper) Explorer {
	explorer := new(azureExplorer)
	explorer.logger = tools.LoggerForContext(ctx, "azureExplorer")
	explorer.keeper = keeper

	return explorer
}

func (bkt *azureExplorer) Word2Url(word string) string {
	return "https://" + word + ".blob.core.windows.net/" + word + "/?restype=container&comp=list"
}

func (bkt *azureExplorer) WordAfix2Urls(word string, afix string) []string {
	urls := make([]string, 0, 13)

	urls = append(urls, "https://"+word+".blob.core.windows.net/"+afix+"/?restype=container&comp=list")

	urls = append(urls, "https://"+word+afix+".blob.core.windows.net/"+word+afix+"/?restype=container&comp=list")
	urls = append(urls, "https://"+word+afix+".blob.core.windows.net/"+word+"/?restype=container&comp=list")
	urls = append(urls, "https://"+word+afix+".blob.core.windows.net/"+afix+"/?restype=container&comp=list")

	urls = append(urls, "https://"+afix+word+".blob.core.windows.net/"+afix+word+"/?restype=container&comp=list")
	urls = append(urls, "https://"+afix+word+".blob.core.windows.net/"+word+"/?restype=container&comp=list")
	urls = append(urls, "https://"+afix+word+".blob.core.windows.net/"+afix+"/?restype=container&comp=list")

	urls = append(urls, "https://"+word+"-"+afix+".blob.core.windows.net/"+word+"-"+afix+"/?restype=container&comp=list")
	urls = append(urls, "https://"+word+"-"+afix+".blob.core.windows.net/"+word+"/?restype=container&comp=list")
	urls = append(urls, "https://"+word+"-"+afix+".blob.core.windows.net/"+afix+"/?restype=container&comp=list")

	urls = append(urls, "https://"+afix+"-"+word+".blob.core.windows.net/"+afix+"-"+word+"/?restype=container&comp=list")
	urls = append(urls, "https://"+afix+"-"+word+".blob.core.windows.net/"+word+"/?restype=container&comp=list")
	urls = append(urls, "https://"+afix+"-"+word+".blob.core.windows.net/"+afix+"/?restype=container&comp=list")

	return urls
}

func (xpl *azureExplorer) Explore(ctx context.Context, words []string, affixes []string) {
	explore(ctx, xpl, words, affixes)
}

func (xpl *azureExplorer) Logger() *zerolog.Logger {
	return xpl.logger
}

func (xpl *azureExplorer) Keeper() *Keeper {
	return xpl.keeper
}

package bucket

import (
	"context"
)

type ObjectType int

const (
	DomainUrl ObjectType = iota
	FileUrl
)

// Fetcher interface for objects that can download http pages asynchronuosly
// in a concurrent non-blocking fashion
type Fetcher interface {
	Fetch(ctx context.Context, obj ...any)
	Stop()
}

package bucket

import (
	"encoding/xml"
	"fmt"
	"time"
)

type GoogleContent struct {
	XMLName      xml.Name  `xml:"Contents"`
	Key          string    `xml:"Key"`
	LastModified time.Time `xml:"LastModified"`
	FileSize     int64     `xml:"Size"`
	StorageClass string    `xml:"StorageClass"`
}

func (element *GoogleContent) Url() string {
	return element.Key
}

func (element *GoogleContent) Size() int64 {
	return element.FileSize
}

func (element *GoogleContent) Created() int64 {
	return element.LastModified.Unix()
}

func (element *GoogleContent) Modified() int64 {
	return element.LastModified.Unix()
}

type GoogleDirectory struct {
	XMLName     xml.Name        `xml:"ListBucketResult"`
	Name        string          `xml:"Name"`
	Prefix      string          `xml:"Prefix"`
	Marker      string          `xml:"Marker"`
	MaxKeys     int64           `xml:"MaxKeys"`
	IsTruncated bool            `xml:"IsTruncated"`
	Contents    []GoogleContent `xml:"Contents"`
}

func (directory *GoogleDirectory) Type() string {
	return "AWS"
}

func (directory *GoogleDirectory) NumberOfElements() int {
	return len(directory.Contents) //int(directory.MaxKeys)
}

func (directory *GoogleDirectory) Element(n int64) Element {
	if n >= directory.MaxKeys {
		return nil
	}
	return &directory.Contents[n]
}

func (directory *GoogleDirectory) Truncated() bool {
	return directory.IsTruncated
}

func (directory *GoogleDirectory) NextMarker() string {
	if !directory.Truncated() {
		return ""
	}
	return directory.Element(int64(len(directory.Contents) - 1)).Url()
}

func (directory *GoogleDirectory) Print() {
	fmt.Println(directory.XMLName)
	fmt.Println(directory.Name)
	fmt.Println(directory.MaxKeys)

}

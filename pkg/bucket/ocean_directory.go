package bucket

import (
	"encoding/xml"
	"fmt"
	"time"
)

type OceanContent struct {
	XMLName      xml.Name  `xml:"Contents"`
	Key          string    `xml:"Key"`
	LastModified time.Time `xml:"LastModified"`
	FileSize     int64     `xml:"Size"`
	StorageClass string    `xml:"StorageClass"`
}

func (element *OceanContent) Url() string {
	return element.Key
}

func (element *OceanContent) Size() int64 {
	return element.FileSize
}

func (element *OceanContent) Created() int64 {
	return element.LastModified.Unix()
}

func (element *OceanContent) Modified() int64 {
	return element.LastModified.Unix()
}

type OceanDirectory struct {
	XMLName     xml.Name       `xml:"ListBucketResult"`
	Name        string         `xml:"Name"`
	Prefix      string         `xml:"Prefix"`
	Marker      string         `xml:"Marker"`
	MaxKeys     int64          `xml:"MaxKeys"`
	IsTruncated bool           `xml:"IsTruncated"`
	Contents    []OceanContent `xml:"Contents"`
}

func (directory *OceanDirectory) Type() string {
	return "AWS"
}

func (directory *OceanDirectory) NumberOfElements() int {
	return len(directory.Contents) //int(directory.MaxKeys)
}

func (directory *OceanDirectory) Element(n int64) Element {
	if n >= directory.MaxKeys {
		return nil
	}
	return &directory.Contents[n]
}

func (directory *OceanDirectory) Truncated() bool {
	return directory.IsTruncated
}

func (directory *OceanDirectory) NextMarker() string {
	if !directory.Truncated() {
		return ""
	}
	return directory.Element(int64(len(directory.Contents) - 1)).Url()
}

func (directory *OceanDirectory) Print() {
	fmt.Println(directory.XMLName)
	fmt.Println(directory.Name)
	fmt.Println(directory.MaxKeys)

}

package bucket

import (
	"context"
	"encoding/xml"
	"fmt"
	"strings"
	"time"

	"gitlab.com/oldmanswar/gotick/pkg/tools"
)

// Element is an interface representing entry in a Directory
type Element interface {
	Url() string
	Size() int64
	Created() int64
	Modified() int64
}

// Directory is an interface representing a list of objects in a data bucket
type Directory interface {
	Type() string
	NumberOfElements() int
	Element(int64) Element
	NextMarker() string
	Truncated() bool
	NextPartial(string) string
	Print()
}

// UrlToDir parses a url and returns a directory of the appropriate type
// Types supported:
//
//	AWS (S3)
//	Google Cloud Storage
//	DigitalOcean Spaces
//	Azure Blob Storage
func UrlToDir(urlString string) (Directory, int64) {
	if urlString[len(urlString)-1] != '/' {
		urlString += "/"
	}
	hash := tools.FindHash(urlString)
	if strings.Contains(urlString, "amazonaws.com") {
		return new(AwsDirectory), hash
	}

	if strings.Contains(urlString, "googleapis.com") {
		return new(AwsDirectory), hash
	}
	if strings.Contains(urlString, "digitaloceanspaces.com") {
		return new(AwsDirectory), hash
	}
	if strings.Contains(urlString, "blob.core.windows.net") {
		return new(AzureDirectory), hash
	}
	return nil, 0
}

// ElementToFile converts an Element into a database record
func ElementToFile(element Element, dirHash int64) *FileRecord {
	record := new(FileRecord)

	record.hash = tools.FindHash(element.Url())
	record.domainHash = dirHash
	record.httpStatus = 0
	record.created = element.Created()
	record.modified = element.Modified()
	record.inspected = time.Now().Unix()
	record.downloaded = 0
	record.size = element.Size()
	record.url = element.Url()

	return record
}

// RetrieveDirectory retrieves the full directory of a bucket to be processed by the Keeper
func RetrieveDirectory(ctx context.Context, keeper *Keeper, domain *DomainRecord) {
	logger := tools.LoggerForContext(ctx, "RetrieveDirectory")
	totalElements := 0
	urlString := domain.Url()
	partialDir := domain.Url()
	for {
		body, status, errString := tools.GetUrl(ctx, partialDir, false)
		logger.Info().Msgf("entries %d retrieving partial directory %s", totalElements, partialDir)
		if status == 200 {
			dir, _ := UrlToDir(urlString)
			err := xml.Unmarshal([]byte(body), &dir)
			if err != nil {
				logger.Error().Msgf("error parsing %s", urlString)
				break
			}
			dirNoE := dir.NumberOfElements()
			logger.Debug().Msgf("elements in partial %d", dirNoE)
			for i := 0; i < dirNoE; i++ {
				logger.Debug().Msgf("partial elements %d/%d", i, dirNoE)
				//fmt.Println(dir.Element(int64(i)))
				totalElements++
				logger.Debug().Msgf("%d %s", totalElements, dir.Element(int64(i)).Url())
				record := ElementToFile(dir.Element(int64(i)), domain.hash)
				msg := fileDomain{file: record, domain: domain}
				keeper.WriteFile(&msg, Write, nil)
			}
			if dir.Truncated() {
				partialDir = dir.NextPartial(urlString)

			} else {
				break
			}
		} else {
			logger.Debug().Msgf("status %d: %s", status, errString)
			fmt.Println(status)
			fmt.Println(body)
			break
		}
	}
	logger.Info().Msgf("finished retrieving %d entries of directory %s", totalElements, urlString)

}

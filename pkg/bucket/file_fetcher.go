package bucket

import (
	"context"
	"net/url"
	"strings"
	"sync"
	"time"

	"github.com/rs/zerolog"
	"gitlab.com/oldmanswar/gotick/pkg/tools"
)

// FileFetcher in an implementation of the Fetcher interface for data objects
type FileFetcher struct {
	logger *zerolog.Logger
	keeper *Keeper
	fechan chan *fileDomain
	cancel context.CancelFunc
	wg     sync.WaitGroup
}

// NewFileFetcher instantiates an asynchronous fetcher for object ulrs
// The downloaded page is redirected to the Keeper for processing
// Concurrency can be regulated through the maxSymul parameter
func NewFileFetcher(ctx context.Context, keeper *Keeper, maxSimul int) *FileFetcher {
	fetcher := new(FileFetcher)
	fetcher.logger = tools.LoggerForContext(ctx, "FileFetcher")
	fetcher.keeper = keeper
	keeper.SetFileFetcher(fetcher)
	fetcher.fechan = make(chan *fileDomain)
	var fCtx context.Context
	fCtx, fetcher.cancel = context.WithCancel(ctx)
	for i := 0; i < maxSimul; i++ {
		go func() {
			fetcher.listener(fCtx)
		}()
	}

	return fetcher
}

// fetch is the workhorse function
func (fetcher *FileFetcher) fetch(ctx context.Context, msg *fileDomain) {
	logger := fetcher.logger
	logger.Debug().Msg("fetching url " + msg.file.url)

	urlStr := msg.domain.url + url.QueryEscape(msg.file.url)

	record := msg.file
	fd := fileDomain{file: record, domain: msg.domain, body: nil}

	retries := 0
	retry := true
	for retry {
		body, status, errString := tools.GetUrl(ctx, urlStr, false)
		record.httpStatus = int16(status)
		record.message = errString

		switch status {
		case 200:
			logger.Info().Int("status", status).Msgf("body %t downloaded %s", body != nil, urlStr)
			record.downloaded = time.Now().Unix()
			record.message = "success"
			retry = false
			fd.body = body
		case 403:
			logger.Info().Int("status", status).Msg("protected file " + urlStr)
			record.downloaded = 0
			record.message = "forbidden"
			retry = false
		case 404:
			logger.Debug().Int("status", status).Msg("not found")
			record.downloaded = 0
			record.message = "file not found"
			retry = false
		default:
			if strings.Contains(errString, "host unreachable") {
				logger.Warn().Msgf("host unreachable: internet down? (%d)", retries)
				logger.Warn().Msg(errString)
				time.Sleep(10 * time.Second)
				if retries < 6 {
					retries++
					continue
				} else {
					record.message = "host unreachable"
					retry = false
				}
			} else if strings.Contains(errString, "access denied") {
				logger.Info().Int("status", status).Msg("protected file " + urlStr)
				record.downloaded = 0
				record.message = "forbidden"
				retry = false
				break
			} else if strings.Contains(errString, "no such host") {
				retry = false
				break
			} else {
				logger.Warn().Msgf("weird stuff %d %s", status, errString)
				retry = false
			}
		}
	}

	fetcher.keeper.WriteFile(&fd, Update, fd.body)
}

// Stop deactivates the fetcher
func (fetcher *FileFetcher) Stop() {
	fetcher.logger.Info().Msg("Stop")
	fetcher.wg.Wait()
	fetcher.cancel()
	fetcher.keeper.RemoveFileFetcher()
	close(fetcher.fechan)
}

// fetch  listens for download requests
func (fetcher *FileFetcher) listener(ctx context.Context) {
	keepGoing := true
	for keepGoing {
		select {
		case msg := <-fetcher.fechan:
			fetcher.fetch(ctx, msg)
			fetcher.wg.Done()
		case <-ctx.Done():
			keepGoing = false
		}
	}
}

// Fetch requests the downloading of a bucket url asynchronously
func (fetcher *FileFetcher) Fetch(ctx context.Context, obj ...any) { //},
	fetcher.wg.Add(1)
	domain := obj[0].(*DomainRecord)
	file := obj[1].(*FileRecord)
	if strings.Contains(domain.url, "?") {
		domain.url = domain.url[:strings.Index(domain.url, "?")]
	}
	fetcher.logger.Debug().Msg("fetch request url " + domain.url + file.url)
	fetcher.fechan <- &fileDomain{domain: domain, file: file}
}

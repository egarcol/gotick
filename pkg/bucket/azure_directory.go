package bucket

import (
	"encoding/xml"
	"fmt"
	"net/http"
)

type AzureProperties struct {
	XMLName      xml.Name `xml:"Properties"`
	LastModified string   `xml:"Last-Modified"`
	FileSize     int64    `xml:"Content-Length"`
	Type         string   `xml:"Content-Type"`
	Encoding     string   `xml:"Content-Encoding"`
	Language     string   `xml:"Content-Language"`
}
type AzureBlob struct {
	XMLName    xml.Name        `xml:"Blob"`
	Name       string          `xml:"Name"`
	Properties AzureProperties `xml:"Properties"`
}

/*
	type AzureBlobs struct {
		//XMLName xml.Name    `xml:"Blobs"`
		Blob []AzureBlob `xml:"Blob"`
	}
*/
type AzureDirectory struct {
	XMLName  xml.Name    `xml:"EnumerationResults"`
	Endpoint string      `xml:"ServiceEndpoint,attr"`
	Name     string      `xml:"ContainerName,attr"`
	NextMark string      `xml:"NextMarker"`
	Blobs    []AzureBlob `xml:"Blobs>Blob"`
}

func (element *AzureBlob) Url() string {
	return element.Name
}

func (element *AzureBlob) Size() int64 {
	return element.Properties.FileSize
}

func (element *AzureBlob) Created() int64 {
	time, _ := http.ParseTime(element.Properties.LastModified)
	return time.Unix()
}

func (element *AzureBlob) Modified() int64 {
	time, _ := http.ParseTime(element.Properties.LastModified)
	return time.Unix()
}

func (directory *AzureDirectory) Type() string {
	return "Azure"
}
func (directory *AzureDirectory) NumberOfElements() int {
	return len(directory.Blobs)
}

func (directory *AzureDirectory) Element(n int64) Element {
	if n >= int64(directory.NumberOfElements()) {
		return nil
	}
	return &directory.Blobs[n]
}

func (directory *AzureDirectory) Truncated() bool {
	return len(directory.NextMark) > 0
}

func (directory *AzureDirectory) NextMarker() string {
	return directory.NextMark
}

func (directory *AzureDirectory) NextPartial(original string) string {
	return original + "&marker=" + directory.NextMark
}

func (directory *AzureDirectory) Print() {
	fmt.Println(directory.XMLName)
	fmt.Println(directory.Name)
	fmt.Println(directory.NextMark)
	fmt.Println(directory.Blobs)
	fmt.Println(directory.Type())
	fmt.Println(directory.NumberOfElements())
	fmt.Println(directory)
}

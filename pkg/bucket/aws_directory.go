package bucket

import (
	"encoding/xml"
	"fmt"
	"net/url"
	"time"
)

type AwsContent struct {
	XMLName      xml.Name  `xml:"Contents"`
	Key          string    `xml:"Key"`
	LastModified time.Time `xml:"LastModified"`
	FileSize     int64     `xml:"Size"`
	StorageClass string    `xml:"StorageClass,omitempty"`
}

func (element *AwsContent) Url() string {
	return element.Key
}

func (element *AwsContent) Size() int64 {
	return element.FileSize
}

func (element *AwsContent) Created() int64 {
	return element.LastModified.Unix()
}

func (element *AwsContent) Modified() int64 {
	return element.LastModified.Unix()
}

type AwsDirectory struct {
	XMLName     xml.Name     `xml:"ListBucketResult"`
	Name        string       `xml:"Name"`
	Prefix      string       `xml:"Prefix"`
	Marker      string       `xml:"Marker"`
	MaxKeys     int64        `xml:"MaxKeys"`
	IsTruncated bool         `xml:"IsTruncated"`
	Contents    []AwsContent `xml:"Contents"`
}

func (directory *AwsDirectory) Type() string {
	return "AWS"
}

func (directory *AwsDirectory) NumberOfElements() int {
	return len(directory.Contents) //int(directory.MaxKeys)
}

func (directory *AwsDirectory) Element(n int64) Element {
	if n >= int64(directory.NumberOfElements()) {
		return nil
	}
	return &directory.Contents[n]
}

func (directory *AwsDirectory) Truncated() bool {
	return directory.IsTruncated
}

func (directory *AwsDirectory) NextMarker() string {
	if !directory.Truncated() {
		return ""
	}
	return directory.Element(int64(len(directory.Contents) - 1)).Url()
}

func (directory *AwsDirectory) NextPartial(original string) string {
	return original + "?marker=" + url.QueryEscape(directory.NextMarker())
}

func (directory *AwsDirectory) Print() {
	fmt.Println(directory.XMLName)
	fmt.Println(directory.Name)
	fmt.Println(directory.MaxKeys)
}
